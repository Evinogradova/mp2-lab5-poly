#include <gtest\gtest.h>
#include "theadring.h"
#include "tmonom.h"

const int N = 5;

TEST(THeadRing, can_create_headring)
{
	ASSERT_NO_THROW(THeadRing headring);
}

TEST(THeadRing, new_headring_list_is_empty)
{
	THeadRing headring;
	EXPECT_TRUE(headring.IsEmpty());
}

TEST(THeadRing, can_ins_after_head)
{
	THeadRing headring;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		headring.InsLast(mon);
	}
	mon = new TMonom(1, 1);
	headring.InsFirst(mon);
	headring.SetCurrentPos(0);
	EXPECT_EQ(*mon, *(PTMonom)headring.GetDatValue());
}

TEST(THeadRing, can_delete_first_link)
{
	THeadRing headring;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		headring.InsLast(mon);
	}
	headring.DelFirst();
	headring.Reset();
	mon = new TMonom(1, 1);
	EXPECT_EQ(*mon, *(PTMonom)headring.GetDatValue());
}