##Методы программирования 2: Полиномы

###Цели и задачи
В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:
- ввод полинома 
- организация хранения полинома
- удаление введенного ранее полинома;
- копирование полинома;
- сложение двух полиномов;
- вычисление значения полинома при заданных значениях переменных;
- вывод.

В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:
- поддержка понятия текущего звена;
- вставка звеньев в начало, после текущей позиции и в конец списков;
- удаление звеньев в начале и в текущей позиции списков;
- организация последовательного доступа к звеньям списка (итератор).

####Условия и ограничения

1.	Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
2.	Степени переменных полиномов не могут превышать значения 9, т.е. 0 <= i, j, k <= 9.
3.	Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).

###Выполнение работы
####Общая структура классов
- класс TDatValue для определения класса объектов-значений списка (абстрактный класс);
- класс TMonom для определения объектов-значений параметров монома;
- класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);
- класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;
- класс TDatList для определения линейных списков;
- класс THeadRing для определения циклических списков с заголовком;
- класс TPolinom для определения полиномов.

![](http://s019.radikal.ru/i622/1704/36/c40730d943b0.png)

####Реализация классов

##### TDatValue:
```cpp
#ifndef __DATVALUE_H__
#define __DATVALUE_H__

#include <iostream>

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue
{
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};

#endif
```
#####TMonom:
```cpp
#ifndef __MONOM_H__
#define __MONOM_H__

#include <iostream>
#include "tdatvalue.h"

class TMonom;
typedef TMonom *PTMonom;

class TMonom : public TDatValue
{
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0) : Coeff(cval), Index(ival) {}
	virtual TDatValue * GetCopy() // изготовить копию
	{
		TDatValue * tmp = new TMonom(Coeff, Index);
		return tmp;
	}
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void)     { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void)     { return Index; }
	TMonom& operator=(const TMonom &tm)
	{
		Coeff = tm.Coeff;
		Index = tm.Index;
		return *this;
	}
	bool operator==(const TMonom &tm) const
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm)
	{
		return Index < tm.Index;
	}

	friend class TPolinom;
};

#endif
```
#####TRootLink:
```cpp
#ifndef __ROOTLINK_H__
#define __ROOTLINK_H__

#include <iostream>
#include "tdatvalue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink
{
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = NULL) : pNext(pN) {}
	PTRootLink  GetNextLink() { return pNext; }
	void SetNextLink(PTRootLink pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink pLink)
	{
		PTRootLink p = pNext;
		pNext = pLink;
		if (pLink != NULL)
			pLink->pNext = p;
	}

	virtual void SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};

#endif
```
#####TDatLink:
```cpp
#ifndef __DATLINK_H__
#define __DATLINK_H__

#include <iostream>
#include "trootlink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink
{
protected:
	PTDatValue pValue;  // указатель на объект значения
public:
	TDatLink(PTDatValue pVal = NULL, PTRootLink pN = NULL) : TRootLink(pN)
	{
		pValue = pVal;
	}
	void SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return pValue; }
	PTDatLink  GetNextDatLink() { return (PTDatLink)pNext; }

	friend class TDatList;
};


#endif
```
#####TDatList:
`TDatList.h`:
```cpp
#ifndef __DATLIST_H__
#define __DATLIST_H__

#include <iostream>
#include "tdatlink.h"

enum TLinkPos {FIRST, CURRENT, LAST};

class TDatList
{
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = NULL, PTDatLink pLink = NULL);
	void DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }
	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty() const { return pFirst == pStop; } // список пуст ?
	int GetListLength() const { return ListLen; }       // к-во звеньев
	// навигация
	void SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual void Reset(void);             // установить на начало списка
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
	// (=1 после применения GoNext для последнего звена списка)
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // перед первым
	virtual void InsLast(PTDatValue pVal = NULL); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = NULL); // перед текущим 
	// удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список
};

#endif
```
`TDatList.cpp`:
```cpp
#include "tdatlist.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList::TDatList()
{
	pFirst = pLast = pStop = nullptr;
	ListLen = 0;
	Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case CURRENT:
		tmp = pCurrLink;
		break;
	case LAST:
		tmp = pLast;
		break;
	}
	return (tmp == nullptr) ? nullptr : tmp->pValue;
}

void TDatList::SetCurrentPos(int pos)
{
	if (pos < ListLen && pos >= 0)
	{
		Reset();
		for (int i = 0; i < pos; i++, GoNext());
	}
	else
		throw "Error";
}

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

void TDatList::Reset(void)
{
	pPrevLink = pStop;
	if (IsEmpty())
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
	else
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
}

bool TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp != nullptr)
	{
		pFirst = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = tmp;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = tmp;
		else
			CurrPos++;
	}
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp != nullptr)
	{
		if (pLast != nullptr)
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (pCurrLink == pFirst || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink tmp = GetLink(pVal, pCurrLink);
		if (tmp != nullptr)
		{
			pPrevLink->SetNextLink(tmp);
			pCurrLink = tmp;
			ListLen++;
		}
	}
}

void TDatList::DelFirst(void)
{
	if (!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		else if (CurrPos > 1)
			CurrPos--;
	}
}

void TDatList::DelCurrent(void)
{
	if (pCurrLink != pStop)
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}
```
#####THeadRing:
`THeadRing.h`:
```cpp
#ifndef __HEADRING_H__
#define __HEADRING_H__

#include <iostream>
#include "tdatlist.h"

class THeadRing : public TDatList
{
protected:
	PTDatLink pHead;     // заголовок, pFirst - звено за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = NULL); // после заголовка
	// удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};

#endif
```
`THeadRing.cpp`:
```cpp
#include "theadring.h"

THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	pStop = pHead;
	Reset();
	ListLen = 0;
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	DelList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal)
{
	TDatList::InsFirst(pVal);
	pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void)
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```
#####TPolinom:
`TPolinom.h`:
```cpp
#ifndef __POLINOM_H__
#define __POLINOM_H__

#include <iostream>
#include "theadring.h"
#include "tmonom.h"

using namespace std;

class TPolinom : public THeadRing
{
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // конструктор
	// полинома из массива «коэффициент-индекс»
	TPolinom(TPolinom &q);      // конструктор копирования
	PTMonom GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	bool operator==(TPolinom &q);
	double CalculatePoly(int x = 0, int y = 0, int z = 0);

	friend ostream & operator<<(ostream & os, TPolinom & q);
};

#endif
```
`TPolinom.cpp`:
```cpp
#include "tpolinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (int i = 0; i < km; i++)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		monom = q.GetMonom();
		InsLast(monom->GetCopy());
	}
}

TPolinom & TPolinom::operator+(TPolinom &q)
{
	PTMonom q_mon, t_mon, r_mon;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		q_mon = q.GetMonom();
		t_mon = GetMonom();
		if (q_mon->Index < t_mon->Index)
		{
			res->InsLast(t_mon->GetCopy());
			GoNext();
		}
		else if (t_mon->Index < q_mon->Index)
		{
			res->InsLast(q_mon->GetCopy());
			q.GoNext();
		}
		else if (q_mon->Index == t_mon->Index)
		{
			if (t_mon->Index == -1)
				flag = false;
			r_mon = new TMonom(q_mon->Coeff + t_mon->Coeff, t_mon->Index);
			if (r_mon->Coeff != 0)
				res->InsLast(r_mon->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator=(TPolinom &q)
{
	if (&q != nullptr && &q != this)
	{
		DelList();
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			monom = q.GetMonom();
			InsLast(monom->GetCopy());
		}
	}
	return *this;
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
	double res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
		}
	}
	return res;
}

bool TPolinom::operator==(TPolinom &q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		
		Reset(); q.Reset();
		PTMonom t_mon, q_mon;
		while (!IsListEnded())
		{
			t_mon = GetMonom();
			q_mon = q.GetMonom();
			if (*t_mon == *q_mon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

ostream & operator<<(ostream & os, TPolinom & q)
{
	if (q.ListLen)
	{
		PTMonom mon;
		int indx, indy, indz;
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			mon = q.GetMonom();
			indx = mon->GetIndex() / 100;
			indy = (mon->GetIndex() % 100) / 10;
			indz = mon->GetIndex() % 10;
			if (mon->GetCoeff() > 0)
				os << "+" << mon->GetCoeff();
			else
				os << mon->GetCoeff();
			if (indx > 0)
				os << "x^" << indx;
			if (indy > 0)
				os << "y^" << indy;
			if (indz > 0)
				os << "z^" << indz;
		}
	}
	else
		os << 0;
	return os;
}
```
####Тесты
#####Test_TDatList:
```cpp
#include <gtest\gtest.h>
#include "tdatlist.h"
#include "tmonom.h"

const int N = 5;


TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList list);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList list;
	EXPECT_TRUE(list.IsEmpty());
}

TEST(TDatList, new_list_has_null_length)
{
	TDatList list;
	EXPECT_EQ(0, list.GetListLength());
}

TEST(TDatList, can_set_current_pos)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.SetCurrentPos(3);
	EXPECT_EQ(3, testlist.GetCurrentPos());
}

TEST(TDatList, can_reset_list)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.Reset();
	EXPECT_EQ(0, testlist.GetCurrentPos());
}

TEST(TDatList, can_go_to_next_link)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.Reset();
	testlist.GoNext();
	EXPECT_EQ(1, testlist.GetCurrentPos());
}

TEST(TDatList, insertion_change_listlen)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	mon = new TMonom(1, 1);
	testlist.InsLast(mon);
	EXPECT_EQ(N + 1, testlist.GetListLength());
}

TEST(TDatList, can_delete_list)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.DelList();
	EXPECT_TRUE(testlist.IsEmpty());
}
```

![](http://s019.radikal.ru/i608/1704/a6/3adae36cd01a.png)

#####Test_THeadRing:
```cpp
#include <gtest\gtest.h>
#include "theadring.h"
#include "tmonom.h"

const int N = 5;

TEST(THeadRing, can_create_headring)
{
	ASSERT_NO_THROW(THeadRing headring);
}

TEST(THeadRing, new_headring_list_is_empty)
{
	THeadRing headring;
	EXPECT_TRUE(headring.IsEmpty());
}

TEST(THeadRing, can_ins_after_head)
{
	THeadRing headring;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		headring.InsLast(mon);
	}
	mon = new TMonom(1, 1);
	headring.InsFirst(mon);
	headring.SetCurrentPos(0);
	EXPECT_EQ(*mon, *(PTMonom)headring.GetDatValue());
}

TEST(THeadRing, can_delete_first_link)
{
	THeadRing headring;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		headring.InsLast(mon);
	}
	headring.DelFirst();
	headring.Reset();
	mon = new TMonom(1, 1);
	EXPECT_EQ(*mon, *(PTMonom)headring.GetDatValue());
}
```

![](http://s016.radikal.ru/i337/1704/57/53ff639d8304.png)

#####Test_TPolinom:
```cpp
#include <gtest\gtest.h>
#include "tpolinom.h"

int monoms[][2] = { {4, 643}, {-1, 501}, {8, 411}, {-15, 21}, {3, 0} };
TPolinom poly(monoms, 5);

TEST(TPolinom, can_create_polinom)
{
	ASSERT_NO_THROW(TPolinom tpol);
}

TEST(TPolinom, can_create_polinom_with_parameters)
{
	ASSERT_NO_THROW(TPolinom tpol(monoms, 5));
}

TEST(TPolinom, equal_polinoms_are_equal)
{
	TPolinom tpol(monoms, 5);
	EXPECT_TRUE(poly == tpol);
}

TEST(TPolinom, can_create_copied_polinom)
{
	ASSERT_NO_THROW(TPolinom copied(poly));
}

TEST(TPolinom, copied_polinom_is_equal_to_origin)
{
	TPolinom copied(poly);
	EXPECT_TRUE(poly == copied);
}

TEST(TPolinom, can_get_current_monom)
{
	poly.Reset();
	poly.GoNext();
	PTMonom mon = new TMonom(-1, 501);
	EXPECT_EQ(*mon, *poly.GetMonom());
}

TEST(TPolinom, can_copy_polinom_by_operator)
{
	TPolinom copied = poly;
	EXPECT_TRUE(copied == poly);
}

TEST(TPolinom, assign_operator_changes_polinom)
{
	int mon[][2] = { {2, 133}, {7, 15} };
	TPolinom tpol(mon, 2);
	tpol = poly;
	EXPECT_TRUE(poly == tpol);
}

TEST(TPolinom, can_sum_polinoms)
{
	int mon1[][2] = { { 2, 455 }, { -1, 400 }, { 8, 3 } };
	int mon2[][2] = { { 7, 601 }, { 5, 455 }, { 1, 400 }, { 3, 0 } };
	int mon_exp[][2] = { { 7, 601 }, { 7, 455 }, { 8, 3 }, { 3, 0 } };
	TPolinom poly1(mon1, 3), poly2(mon2, 4), poly_exp(mon_exp, 4), polyr;
	polyr = poly1 + poly2;
	EXPECT_TRUE(polyr == poly_exp);
}

TEST(TPolinom, can_calculate_polinom)
{
	int x = 1, y = 1, z = 1;
	EXPECT_EQ(-1, poly.CalculatePoly(x, y, z));
}
```

![](http://s019.radikal.ru/i629/1704/a0/bc9c44071bd8.png)

####Демонстрационная программа
```cpp
#include "tpolinom.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	int ms1[][2] = { { 1, 543 }, { 3, 432 }, { 5, 321 }, { 7, 210 }, { 9, 100 } };
	TPolinom poly1(ms1, 5);
	cout << "Полином 1:" << endl << poly1 << endl;

	int ms2[][2] = { { 2, 643 }, { 4, 431 }, { -8, 110 }, { 10, 50 } };
	TPolinom poly2(ms2, 4);
	cout << "Полином 2:" << endl << poly2 << endl;

	TPolinom sum_poly = poly1 + poly2;
	cout << "Сумма полиномов:" << endl << sum_poly << endl;

	int x, y, z;
	cout << "Введите значения переменных:";
	cin >> x >> y >> z;
	cout << sum_poly << "=" << sum_poly.CalculatePoly(x, y, z) << endl;

	return 0;
}
```

![](http://s009.radikal.ru/i308/1704/25/d1a4bf0c72ec.png)



###Вывод

В ходе выполнения данной работы были получены навыки работы с принципиально новой структурой данных "список".

