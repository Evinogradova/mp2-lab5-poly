#include <gtest\gtest.h>
#include "tpolinom.h"

int monoms[][2] = { {4, 643}, {-1, 501}, {8, 411}, {-15, 21}, {3, 0} };
TPolinom poly(monoms, 5);

TEST(TPolinom, can_create_polinom)
{
	ASSERT_NO_THROW(TPolinom tpol);
}

TEST(TPolinom, can_create_polinom_with_parameters)
{
	ASSERT_NO_THROW(TPolinom tpol(monoms, 5));
}

TEST(TPolinom, equal_polinoms_are_equal)
{
	TPolinom tpol(monoms, 5);
	EXPECT_TRUE(poly == tpol);
}

TEST(TPolinom, can_create_copied_polinom)
{
	ASSERT_NO_THROW(TPolinom copied(poly));
}

TEST(TPolinom, copied_polinom_is_equal_to_origin)
{
	TPolinom copied(poly);
	EXPECT_TRUE(poly == copied);
}

TEST(TPolinom, can_get_current_monom)
{
	poly.Reset();
	poly.GoNext();
	PTMonom mon = new TMonom(-1, 501);
	EXPECT_EQ(*mon, *poly.GetMonom());
}

TEST(TPolinom, can_copy_polinom_by_operator)
{
	TPolinom copied = poly;
	EXPECT_TRUE(copied == poly);
}

TEST(TPolinom, assign_operator_changes_polinom)
{
	int mon[][2] = { {2, 133}, {7, 15} };
	TPolinom tpol(mon, 2);
	tpol = poly;
	EXPECT_TRUE(poly == tpol);
}

TEST(TPolinom, can_sum_polinoms)
{
	int mon1[][2] = { { 2, 455 }, { -1, 400 }, { 8, 3 } };
	int mon2[][2] = { { 7, 601 }, { 5, 455 }, { 1, 400 }, { 3, 0 } };
	int mon_exp[][2] = { { 7, 601 }, { 7, 455 }, { 8, 3 }, { 3, 0 } };
	TPolinom poly1(mon1, 3), poly2(mon2, 4), poly_exp(mon_exp, 4), polyr;
	polyr = poly1 + poly2;
	EXPECT_TRUE(polyr == poly_exp);
}

TEST(TPolinom, can_calculate_polinom)
{
	int x = 1, y = 1, z = 1;
	EXPECT_EQ(-1, poly.CalculatePoly(x, y, z));
}