#include <gtest\gtest.h>
#include "tdatlist.h"
#include "tmonom.h"

const int N = 5;


TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList list);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList list;
	EXPECT_TRUE(list.IsEmpty());
}

TEST(TDatList, new_list_has_null_length)
{
	TDatList list;
	EXPECT_EQ(0, list.GetListLength());
}

TEST(TDatList, can_set_current_pos)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.SetCurrentPos(3);
	EXPECT_EQ(3, testlist.GetCurrentPos());
}

TEST(TDatList, can_reset_list)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.Reset();
	EXPECT_EQ(0, testlist.GetCurrentPos());
}

TEST(TDatList, can_go_to_next_link)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.Reset();
	testlist.GoNext();
	EXPECT_EQ(1, testlist.GetCurrentPos());
}

TEST(TDatList, insertion_change_listlen)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	mon = new TMonom(1, 1);
	testlist.InsLast(mon);
	EXPECT_EQ(N + 1, testlist.GetListLength());
}

TEST(TDatList, can_delete_list)
{
	TDatList testlist;
	PTMonom mon;
	for (int i = 0; i < N; i++)
	{
		mon = new TMonom(i, i);
		testlist.InsLast(mon);
	}
	testlist.DelList();
	EXPECT_TRUE(testlist.IsEmpty());
}