#include "tdatlist.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
}

TDatList::TDatList()
{
	pFirst = pLast = pStop = nullptr;
	ListLen = 0;
	Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
	PTDatLink tmp = nullptr;
	switch (mode)
	{
	case FIRST:
		tmp = pFirst;
		break;
	case CURRENT:
		tmp = pCurrLink;
		break;
	case LAST:
		tmp = pLast;
		break;
	}
	return (tmp == nullptr) ? nullptr : tmp->pValue;
}

void TDatList::SetCurrentPos(int pos)
{
	if (pos < ListLen && pos >= 0)
	{
		Reset();
		for (int i = 0; i < pos; i++, GoNext());
	}
	else
		throw "Error";
}

int TDatList::GetCurrentPos(void) const
{
	return CurrPos;
}

void TDatList::Reset(void)
{
	pPrevLink = pStop;
	if (IsEmpty())
	{
		pCurrLink = pStop;
		CurrPos = -1;
	}
	else
	{
		pCurrLink = pFirst;
		CurrPos = 0;
	}
}

bool TDatList::IsListEnded(void) const
{
	return pCurrLink == pStop;
}

int TDatList::GoNext(void)
{
	if (!IsListEnded())
	{
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
		return 1;
	}
	else
		return 0;
}

void TDatList::InsFirst(PTDatValue pVal)
{
	PTDatLink tmp = GetLink(pVal, pFirst);
	if (tmp != nullptr)
	{
		pFirst = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pLast = tmp;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = tmp;
		else
			CurrPos++;
	}
}

void TDatList::InsLast(PTDatValue pVal)
{
	PTDatLink tmp = GetLink(pVal, pStop);
	if (tmp != nullptr)
	{
		if (pLast != nullptr)
			pLast->SetNextLink(tmp);
		pLast = tmp;
		ListLen++;
		if (ListLen == 1)
		{
			pFirst = tmp;
			Reset();
		}
		if (IsListEnded())
			pCurrLink = tmp;
	}
}

void TDatList::InsCurrent(PTDatValue pVal)
{
	if (pCurrLink == pFirst || IsEmpty())
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else
	{
		PTDatLink tmp = GetLink(pVal, pCurrLink);
		if (tmp != nullptr)
		{
			pPrevLink->SetNextLink(tmp);
			pCurrLink = tmp;
			ListLen++;
		}
	}
}

void TDatList::DelFirst(void)
{
	if (!IsEmpty())
	{
		PTDatLink tmp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(tmp);
		ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		else if (CurrPos > 1)
			CurrPos--;
	}
}

void TDatList::DelCurrent(void)
{
	if (pCurrLink != pStop)
	{
		if (pCurrLink == pFirst)
			DelFirst();
		else if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
	}
}

void TDatList::DelList(void)
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}
