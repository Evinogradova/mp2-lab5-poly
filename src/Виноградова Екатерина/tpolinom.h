#ifndef __POLINOM_H__
#define __POLINOM_H__

#include <iostream>
#include "theadring.h"
#include "tmonom.h"

using namespace std;

class TPolinom : public THeadRing
{
public:
	TPolinom(int monoms[][2] = NULL, int km = 0); // �����������
	// �������� �� ������� ������������-������
	TPolinom(TPolinom &q);      // ����������� �����������
	PTMonom GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // �������� ���������
	TPolinom & operator=(TPolinom &q); // ������������
	bool operator==(TPolinom &q);
	double CalculatePoly(int x = 0, int y = 0, int z = 0);

	friend ostream & operator<<(ostream & os, TPolinom & q);
};

#endif