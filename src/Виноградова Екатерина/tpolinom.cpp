#include "tpolinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (int i = 0; i < km; i++)
	{
		monom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(monom);
	}
}

TPolinom::TPolinom(TPolinom &q)
{
	PTMonom monom = new TMonom(0, -1);
	pHead->SetDatValue(monom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		monom = q.GetMonom();
		InsLast(monom->GetCopy());
	}
}

TPolinom & TPolinom::operator+(TPolinom &q)
{
	PTMonom q_mon, t_mon, r_mon;
	bool flag = true;
	TPolinom* res = new TPolinom();
	q.Reset();
	Reset();
	while (flag)
	{
		q_mon = q.GetMonom();
		t_mon = GetMonom();
		if (q_mon->Index < t_mon->Index)
		{
			res->InsLast(t_mon->GetCopy());
			GoNext();
		}
		else if (t_mon->Index < q_mon->Index)
		{
			res->InsLast(q_mon->GetCopy());
			q.GoNext();
		}
		else if (q_mon->Index == t_mon->Index)
		{
			if (t_mon->Index == -1)
				flag = false;
			r_mon = new TMonom(q_mon->Coeff + t_mon->Coeff, t_mon->Index);
			if (r_mon->Coeff != 0)
				res->InsLast(r_mon->GetCopy());
			GoNext();
			q.GoNext();
		}
	}
	return *res;
}

TPolinom & TPolinom::operator=(TPolinom &q)
{
	if (&q != nullptr && &q != this)
	{
		DelList();
		PTMonom monom = new TMonom(0, -1);
		pHead->SetDatValue(monom);
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			monom = q.GetMonom();
			InsLast(monom->GetCopy());
		}
	}
	return *this;
}

double TPolinom::CalculatePoly(int x, int y, int z)
{
	double res = 0;
	PTMonom mon;
	int indx, indy, indz;
	if (ListLen)
	{
		for (Reset(); !IsListEnded(); GoNext())
		{
			mon = GetMonom();
			indx = mon->Index / 100;
			indy = (mon->Index % 100) / 10;
			indz = mon->Index % 10;
			res += mon->Coeff*pow(x, indx)*pow(y, indy)*pow(z, indz);
		}
	}
	return res;
}

bool TPolinom::operator==(TPolinom &q)
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		
		Reset(); q.Reset();
		PTMonom t_mon, q_mon;
		while (!IsListEnded())
		{
			t_mon = GetMonom();
			q_mon = q.GetMonom();
			if (*t_mon == *q_mon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

ostream & operator<<(ostream & os, TPolinom & q)
{
	if (q.ListLen)
	{
		PTMonom mon;
		int indx, indy, indz;
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			mon = q.GetMonom();
			indx = mon->GetIndex() / 100;
			indy = (mon->GetIndex() % 100) / 10;
			indz = mon->GetIndex() % 10;
			if (mon->GetCoeff() > 0)
				os << "+" << mon->GetCoeff();
			else
				os << mon->GetCoeff();
			if (indx > 0)
				os << "x^" << indx;
			if (indy > 0)
				os << "y^" << indy;
			if (indz > 0)
				os << "z^" << indz;
		}
	}
	else
		os << 0;
	return os;
}
